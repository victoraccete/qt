#include <QCoreApplication>
#include "controle.h"

int main(int argc, char *argv[])
{
    QCoreApplication a(argc, argv);
    Controle *Bottler = new Controle(&a);
    qDebug() << "Starting process.";
    Bottler->plasticReady(true);
    return a.exec();
}
