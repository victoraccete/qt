#include "controle.h"

Controle::Controle(QObject *parent): QObject(parent)
{
    m_Acontrol = false;
    m_Bcontrol = false;
    m_Ccontrol = false;
}

Controle::~Controle()
{

}

bool Controle::getAControl() const
{
    return m_Acontrol;
}

bool Controle::getBControl() const
{
    return m_Bcontrol;
}

bool Controle::getCControl() const
{
    return m_Ccontrol;
}

void Controle::setAControl(bool value)
{
    m_Acontrol = value;
    emit this->AChanged(value);
}

void Controle::setBControl(bool value)
{
    m_Bcontrol = value;
    emit this->BChanged(value);
}

void Controle::setCControl(bool value)
{
    m_Ccontrol = value;
    emit this->CChanged(value);
}

void Controle::plasticReady(bool ready)
{
    S1();
}

void Controle::S1()
{
    setAControl(true);
    setBControl(true);
    setCControl(false);
    QTimer::singleShot(5000, this, SLOT(S2()));
    qDebug() << "A is open. B is closed. C has no pressure.";
}

void Controle::S2()
{
    setAControl(false);
    setBControl(false);
    setCControl(true);
    qDebug() <<"A is closed. B is open. C has pressure.";
    QTimer::singleShot(500, this, SLOT(S3()));
}

void Controle::S3()
{
    setAControl(false);
    setBControl(true);
    QTimer::singleShot(4000, this, SLOT(S1()));
    qDebug() << "Bottle ready.";
}



