#-------------------------------------------------
#
# Project created by QtCreator 2015-05-20T09:55:46
#
#-------------------------------------------------

QT       += core

QT       -= gui

TARGET = BottlerQT
CONFIG   += console
CONFIG   -= app_bundle

TEMPLATE = app


SOURCES += main.cpp \
    controle.cpp

HEADERS += \
    controle.h
