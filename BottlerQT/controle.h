#ifndef CONTROLE_H
#define CONTROLE_H
#include <QObject>
#include <QTimer>
#include <QDebug>

class Controle: public QObject
{
    Q_OBJECT
    Q_PROPERTY(bool A_control READ getAControl WRITE setAControl NOTIFY AChanged)
    Q_PROPERTY(bool B_control READ getBControl WRITE setBControl NOTIFY BChanged)
    Q_PROPERTY(bool C_control READ getCControl WRITE setCControl NOTIFY CChanged)
public:
    Controle(QObject *parent = 0);
    ~Controle();
    bool getAControl() const;
    bool getBControl() const;
    bool getCControl() const;

signals:
    void AChanged(bool value);
    void BChanged(bool value);
    void CChanged(bool value);

private:
    bool m_Acontrol;
    bool m_Bcontrol;
    bool m_Ccontrol;

public slots:
    void plasticReady(bool ready);
    void S1();
    void S2();
    void S3();

private slots:
    void setAControl(bool value);
    void setBControl(bool value);
    void setCControl(bool value);

};

#endif // CONTROLE_H
